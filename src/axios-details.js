import axios from 'axios';

const instance = axios.create({
    baseURL:process.env.REACT_APP_API_URL,
    // baseURL:'http://194.59.165.34:9000'
    // baseURL: 'https://apptransition.com/aapi' 
})

export default instance;