import React, { Component,Suspense } from 'react';
import './App.css';
import { Route,Switch } from 'react-router-dom';
// import mainbuilder from './containers/mainbuilder/mainbuilder';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import './App.css';

import * as actions from './store/actions/index';


import Layout from './components/Layout/Layout';

//Pages
const Profile = React.lazy(() =>{
  return import('./containers/Profile');
});

const SelectQuestions = React.lazy(() =>{
  return import('./containers/SelectQuestions');
});

const Questions = React.lazy(() =>{
  return import('./containers/Questions');
});

const AllApps = React.lazy(() =>{
  return import('./containers/AllApps');
});

const Responses = React.lazy(() =>{
  return import('./containers/Response.js')
})

const Admin = React.lazy(() =>{
  return import('./containers/Admin')
})

const Clients = React.lazy(() =>{
  return import('./containers/Clients')
})

const Users = React.lazy(() =>{
  return import('./containers/Users')
})

const Insights = React.lazy(() =>{
  return import('./containers/Insights.js');
})

const MailSetting = React.lazy(() =>{
  return import('./containers/MailSetting.js');
})

// const Response = React.lazy(() =>{
//   return import('./containers/Response');
// });

const Login = React.lazy(() =>{
  return import('./containers/Auth/Login.js');
});

const ForgotPassword = React.lazy(() =>{
  return import('./containers/Auth/ForgotPassword');
});


const Logout = React.lazy(() =>{
  return import('./containers/Auth/Logout');
})



class App extends Component {
  

  componentDidMount(){
    this.props.onTryLogin();
    this.props.checkAssessment();
    this.props.checkMapping();
  }

  render() {
    //console.log(this.props.isAuth);
    let layout = (
      <Layout topbar={false} sidebar = {false}  footer = {false} isloginpage={true}>
        <Suspense fallback={<p>Loading....</p>}>
          <Switch>  
            <Route path="/forgot_password"  render={(props) => <ForgotPassword  {...props} /> } />
            <Route path="/"       render={(props) => <Login  {...props} /> } />
          </Switch>
        </Suspense>
      </Layout>
    );
    if(this.props.isAuth){
      if(parseInt(this.props.accessLevel) === 1){
        // profile User
        layout = (
          <Layout topbar={true} sidebar = {true}  footer = {true} isloginpage={false}>
            <Suspense fallback={<p>Loading....</p>}>
              <Switch>  
                <Route path='/logout' render={(props) => <Logout {...props} />} />
                
                <Route path='/select_questions' render={(props) => <SelectQuestions  {...props} /> } />

                <Route path='/' render={(props) => <Profile  {...props} /> } />
              </Switch>
            </Suspense>
          </Layout>);
      }
      else if(parseInt(this.props.accessLevel) === 2){
        // Question User
        layout = (
          <Layout topbar={true} sidebar = {true}  footer = {true} isloginpage={false}>
            <Suspense fallback={<p>Loading....</p>}>
              <Switch>  
                <Route path='/logout' render={(props) => <Logout {...props} />} />
  
                <Route path='/' render={(props) => <Questions  {...props} /> } />
              </Switch>
            </Suspense>
          </Layout>);
      }
      else{
        // super admin
        // access level 0 or 3
        layout = (
          <Layout topbar={true} sidebar = {true}  footer = {true} isloginpage={false}>
            <Suspense fallback={<p>Loading....</p>}>
              <Switch>  
                <Route path='/logout' render={(props) => <Logout {...props} />} />
  
                <Route path='/questions' render={(props) => <Questions  {...props} /> } />
  
                <Route path='/apps' render={(props) => <AllApps  {...props} /> } />
                <Route path='/responses' render={(props) => <Responses  {...props} /> } />

                <Route path='/clients' render={(props) => <Clients  {...props} /> } />
                <Route path='/users' render={(props) => <Users  {...props} /> } />
  
                <Route path='/sub_admin' render={(props) => <Admin  {...props} /> } />

                <Route path='/insights' render={(props) => <Insights  {...props} /> } />

                <Route path='/mail_setting' render={(props) => <MailSetting  {...props} /> } />

                <Route path='/select_questions' render={(props) => <SelectQuestions  {...props} /> } />
                <Route path='/' render={(props) => <Profile  {...props} /> } />
              </Switch>
            </Suspense>
          </Layout>);
      }
      
    }
    return (
      <div className="App">
        {layout}
      </div>
    );
  }
}
const mapStatetoProps = state =>{
  return {
      isAuth: state.auth.token,
      accessLevel:state.auth.accessLevel,
  };
}

const mapDispatchToProps = dispatch =>{
  return {
    onTryLogin: () => dispatch(actions.authCheckStatus()),
    checkMapping: () => dispatch(actions.checkMapping()),
    checkAssessment: () => dispatch(actions.checkAssessment())
  }
}

export default withRouter(connect(mapStatetoProps, mapDispatchToProps)(App))  ;
