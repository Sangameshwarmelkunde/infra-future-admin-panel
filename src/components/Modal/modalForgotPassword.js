import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

// import AUX from '../../hoc/Aux_';
import axios from '../../axios-details';

import Spinner from '../../components/Spinner/simpleSpinner';
import Alert from '../../components/Alert/common';

class modalForgotPassword extends Component {

    constructor(props) {
        super(props);
        this.state={
          email:'',
          loading:false,
          message:null,
          error:null,
        };
    }

    inputChangeHandler = (event) =>{
      this.setState({email:event.target.value});
    }

    closeModal = (time) =>{
      setTimeout(
        () => this.props.toggle(this.state.update)
        ,time);
    }
    submitHandler = (e) =>{
        this.setState({loading:true, error:null, message:null})
        let token ={
            email:this.state.email,
            path:window.location.origin
        }
        axios.post('/forgot/', token)
          .then(res=>{
                //console.log(res.data);
                if(res.data.status){
                    this.setState({loading:false,update:true,message:"A reset link has been sent to your registered email. Please use it to reset your password."});
                }
                else
                    this.setState({loading:false,error:res.data.message});
                this.closeModal(1000);
            }).catch(err =>{
                //console.log(err);
                this.setState({loading:false, error:"Something went wrong.Please try again"})
                this.closeModal(800);
        })
    }
    
    render() {
        let body = (
        <div className="card-body">
            <form onSubmit={this.submitHandler}>
                {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} />: null}
                {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} />: null}
                {this.state.loading ?  <Spinner /> : null }
                <div className="row">
                    <div className="col-sm-12 col-md-12 col-lg-12">
                        <div className="form-group">
                            <label htmlFor="email">Email address</label>
                            <input 
                            type="email"   
                            className="form-control" 
                            placeholder="Enter registered email id"
                            required  
                            onChange={(event) => this.inputChangeHandler(event)}  
                            id="email" />
                        </div>
                    </div>
                    <div className="col-sm-12 col-md-12 col-lg-12" >
                        <div className="form-group" >
                            <br />
                            <button type="submit" className="btn btn-primary waves-effect waves-light">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    );
    
    return (
      <div>
        <Modal size="md" isOpen={this.props.modal} toggle={() => this.props.toggle(this.state.update)} >
          <ModalHeader toggle={() => this.props.toggle(this.state.update)}>Forgot Password</ModalHeader>
          <ModalBody>
            {this.state.loading ?   <Spinner /> : body }
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={() => this.props.toggle(this.state.update)}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}



export default modalForgotPassword;
