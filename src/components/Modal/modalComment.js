import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';


import axios from '../../axios-details';
import Alert from '../Alert/common';
import SimpleSpinner from '../../components/Spinner/simpleSpinner';

class ModalComment extends Component {

    constructor(props) {
        super(props);
        this.state={
            error:'',
            message:'',
            loading:true,
            comment:'',
        };
    }

    componentDidMount (){
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        //console.log(this.props);
        axios.get('/apps/comment/'+this.props.app_id+'/'+this.props.quest_id, config, )
          .then(res => {
            //console.log(res.data);
            if(res.data.status){
              this.setState({loading: false, comment: res.data.data.comment})
            }
            else{
              this.setState({loading: false})
            }
            //this.closeModal();
          })
          .catch( err =>{
            this.setState({loading: false, error: "Something went wrong. Please try again."})
            //this.closeModal();
        });   
    }

   

 
    
    render() {
        let body = (
            <div className="card-body">
                <form onSubmit={this.submitHandler}>
                    {this.state.error ? <Alert msg={this.state.error} classes="alert-danger" /> : null }
                    {this.state.message ? <Alert msg={this.state.message} classes="alert-success" /> : null }
                    {this.state.loading ?  <SimpleSpinner /> : null }
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="form-group">
                                <textarea 
                                    rows="5"
                                    className="form-control" 
                                    required 
                                    value={this.state.comment}
                                    onChange={(event) => this.inputChangeHandler(event,"comment" )}
                                    placeholder="Comment" 
                                    disabled/>
                            </div>
                            
                        </div>
                    </div>
                </form>
            </div>
        );
        return (
        <div>
            <Modal isOpen={this.props.modal} toggle={this.props.toggle} >
            <ModalHeader >Comment</ModalHeader>
            <ModalBody>
                {body}
            </ModalBody>
            <ModalFooter>
                <Button color="secondary" onClick={this.props.toggle}>Cancel</Button>
            </ModalFooter>
            </Modal>
        </div>
        );
    }
}

const mapStateToProps = state =>{
  return {
      token : state.auth.token
  }
}

export default connect(mapStateToProps)(ModalComment);

