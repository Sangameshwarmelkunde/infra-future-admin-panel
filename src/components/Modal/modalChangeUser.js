import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';

import axios from '../../axios-details';
import Spinner from '../../components/Spinner/simpleSpinner';
import Alert from '../../components/Alert/common';
import Star from '../../components/Required/star';


class ChangeUser extends Component {

    constructor(props) {
        super(props);
        this.state={
          client: '',
          options: ['A','B'],
          loading:false,
          message:null,
          error:null,
          update:false,
        };
    }

    fetchUsers = () =>{
        this.setState({ loading:true, error:null, message: null});
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        axios.get('/user/userclient', config)
            .then(res => {
                //console.log(res.data)
                if(res.data.status){
                    this.setState({ loading:false, options:res.data.data.clients})
                }
                else{
                    this.setState({loaded:false});
                }
            })
            .catch( err => {
                this.setState({loading:false})
            });    
    }

    componentDidMount(){
        //console.log(this.props.profileId);
        this.fetchUsers();
    }

    inputChangeHandler = (event, controlName) =>{
        this.setState({[controlName]:event.target.value});
    }

    closeModal = () =>{
      setTimeout(
        () => this.props.toggle(this.state.update)
        ,700);
    }

    submitHandler = (e) =>{
        e.preventDefault();
        //console.log(this.state);
        const apps = {
            token: this.props.token,
            app_id: this.props.app_id.toString(),
            client_id: this.state.client
        }
        //console.log(apps);
        axios.post('/apps/change', apps)
        .then(res=>{
            //console.log(res.data);
            if(res.data.status){
            this.setState({loading:false,update:true, message:res.data.message});
            }
            else {
            this.setState({loading:false, error:res.data.message});
            }
            this.closeModal();
        }).catch(err =>{
            this.setState({loading:false, error:"Something went wrong Please try again"});
            this.closeModal();
        })
    }
    
    render() {
        //console.log(this.state.options);
        let body = (
        <div className="card-body">
            <form onSubmit={this.submitHandler}>
                {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} />: null}
                {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} />: null}
                
                <div className="row">
                    <div className="col-sm-12">
                        <div className="form-group">
                            <label htmlFor="client">Select User/Client<Star /></label>
                            <select 
                              className="form-control" 
                              onChange={(event) => this.inputChangeHandler(event,"client" )}
                              value={this.state.client}>
                                <option>Select</option>
                                {
                                  this.state.options.map((option, index) => {
                                    if(option.active === '1')
                                      return (<option key={index} value={option.clnt_id}>{option.f_name+" "+(option && option.l_name ? option.l_name : '')}</option>)

                                    return option;
                                  })
                                }
                            </select>
                        </div>
                        <button type="submit" className="btn btn-primary waves-effect waves-light">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    );
    
    return (
      <div>
        <Modal isOpen={this.props.modal} toggle={() => this.props.toggle(this.state.update)} >
          <ModalHeader toggle={() => this.props.toggle(this.state.update)}>Change User/Client</ModalHeader>
          <ModalBody>
            {this.state.loading ? <Spinner />: body }
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={() => this.props.toggle(this.state.update)}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStatetoProps = state =>{
  return {
      token: state.auth.token,
  };
}

export default connect(mapStatetoProps)(ChangeUser);
