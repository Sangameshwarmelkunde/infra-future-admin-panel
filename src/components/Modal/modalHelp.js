import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';

class ModalApp extends Component {

    constructor(props) {
        super(props);
        this.state={

      };
    }
    render() {
        return (
        <div>
            <Modal isOpen={this.props.modal} toggle={this.props.toggle} >
            <ModalHeader toggle={this.props.toggle}>Help</ModalHeader>
            <ModalBody>
                Help Content
            </ModalBody>
            <ModalFooter>
                <Button color="secondary" onClick={this.props.toggle}>Cancel</Button>
            </ModalFooter>
            </Modal>
        </div>
        );
    }
}

const mapStateToProps = state =>{
  return {
      token : state.auth.token
  }
}

export default connect(mapStateToProps)(ModalApp);

