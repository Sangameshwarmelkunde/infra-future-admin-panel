import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';


import axios from '../../axios-details';
import Alert from '../Alert/common';
import SimpleSpinner from '../../components/Spinner/simpleSpinner';

class ModalApp extends Component {

    constructor(props) {
        super(props);
        this.state={
            error:'',
            message:'',
            loading:false,
            comment:'',
      };
    }

    inputChangeHandler = (event, controlName) =>{
    //console.log(controlName);
    this.setState({[controlName]:event.target.value});
    }

    submitHandler = (e) =>{
        e.preventDefault();
        this.setState({loading: true, error:null, message:null})
        const contact = {
          token: this.props.token,
          message: this.state.comment,
        }
        //console.log(contact);
        axios.post('/contact', contact)
          .then(res => {
            //console.log(res.data);
            if(res.data.status){
              this.setState({loading: false, message: res.data.message})
            }
            else{
              this.setState({loading: false, error: res.data.message})
            }
            //this.closeModal();
          })
          .catch( err =>{
            this.setState({loading: false, error: "Something went wrong. Please try again."})
            //this.closeModal();
          });   
        
      }
    closeModal = () =>{
    setTimeout(
        this.props.toggle()
        ,700);
    }
    render() {
        let toShow = (<div className="row">
        <div className="col-sm-12">
            <div className="form-group">
                <textarea 
                    rows="5"
                    className="form-control" 
                    required 
                    value={this.state.comment}
                    onChange={(event) => this.inputChangeHandler(event,"comment" )}
                    placeholder="Need help? Write here...." />
            </div>
            <button type="submit" className="btn btn-primary waves-effect waves-light">Submit</button>
        </div>
    </div>);
        let body = (
            <div className="card-body">
                <form onSubmit={this.submitHandler}>
                    {this.state.error ? <Alert msg={this.state.error} classes="alert-danger" /> : null }
                    {this.state.message ? <Alert msg={this.state.message} classes="alert-success" /> : null }
                    {this.state.loading ?  <SimpleSpinner /> : toShow }
                    
                </form>
            </div>
        );
        return (
        <div>
            <Modal isOpen={this.props.modal} toggle={this.props.toggle} >
            <ModalHeader toggle={this.props.toggle}>Contact Us</ModalHeader>
            <ModalBody>
                {body}
            </ModalBody>
            <ModalFooter>
                <Button color="secondary" onClick={this.props.toggle}>Cancel</Button>
            </ModalFooter>
            </Modal>
        </div>
        );
    }
}

const mapStateToProps = state =>{
  return {
      token : state.auth.token
  }
}

export default connect(mapStateToProps)(ModalApp);

