import React from 'react';
import './simpleSpinner.css';

const simpleSpinner = () =>(
    <center>
        <div className="loader"></div>
    </center>
)

export default simpleSpinner;