import React from 'react';

const error = (props) =>(
    <div className="alert alert-danger alert-dismissible mb-0 fade show" role="alert">
        <button type="button" className="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {props.error}
    </div>
)

export default error;

