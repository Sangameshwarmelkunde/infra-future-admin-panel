import React from 'react';

const success = (props) =>(
    <div className="alert alert-success alert-dismissible mb-0 fade show" role="alert">
        <button type="button" className="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {props.success}
    </div>
)

export default success;

