
import React from 'react';
import { Link } from 'react-router-dom';
import AUX from '../../hoc/Aux_';

import HelpModal from '../Modal/modalHelp';
import ContactModal from '../Modal/modalContact';

class  topchart extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            open: true,
            toggle:false,
            toggleContact:false
        }
    }

    check = () =>{
        if(this.state.open){
            document.body.classList.add('enlarged');
        }
        else{
            document.body.classList.remove('enlarged');
        }
        this.setState(prevState =>{
            return{open: !prevState.open}
        })
    }

    toggleModal = (id) => {
        if(id === 1){
            this.setState(prevState =>{
                return{toggle: !prevState.toggle}
            })
        }
        else if(id === 2){
            this.setState(prevState =>{
                return{toggleContact: !prevState.toggleContact}
            })
        }
        
    }
    render() {
        return (
            <AUX>
            <div className="topbar">
                <div className="topbar-left">
                    <Link to="/" className="logo" style={{textTransform:'none'}}>
                        <span>
                            <span style={{color: 'white'}}> {process.env.REACT_APP_WEBSITE_NAME}</span>
                        </span>
                        {/* <i>
                            <label style={{color:'white',fontWeight:900}}>AT</label>
                        </i> */}
                    </Link>
                </div>
                <nav className="navbar-custom">

                    <ul className="navbar-right d-flex list-inline float-right mb-0">
                        {/* <li className="dropdown notification-list d-none d-sm-block">
                            <form role="search" className="app-search">
                                <div className="form-group mb-0"> 
                                    <input type="text" className="form-control" placeholder="Search.." />
                                    <button type="submit"><i className="fa fa-search"></i></button>
                                </div>
                            </form> 
                        </li> */}
                        <li >
                            <button style={{border: '1px solid'}} onClick={() =>this.toggleModal(2)}  className="btn btn-primary w-xs waves-effect waves-light mt-3">Contact Us</button>&nbsp;
                        </li>
                        <li >
                            <button style={{border: '1px solid'}} onClick={() =>this.toggleModal(1)}  className="btn btn-light w-xs waves-effect waves-light mt-3">Help</button>
                        </li>
                        <li className="dropdown notification-list">
                            <div className="dropdown notification-list nav-pro-img">
                                <button className="btn btn-link dropdown-toggle nav-link arrow-none waves-effect nav-user" data-toggle="dropdown"   aria-haspopup="false" aria-expanded="false">
                                    <img src={`${process.env.PUBLIC_URL}/assets/images/users/user.png`} alt="user" className="rounded-circle" />
                                </button>
                                <div className="dropdown-menu dropdown-menu-right profile-dropdown ">
                                    
                                    {/* <button className="btn btn-link dropdown-item" ><i className="mdi mdi-account-circle m-r-5"></i> Profile</button> */}
                                    <Link to="/logout">
                                        <button className="btn btn-link dropdown-item text-danger" ><i className="mdi mdi-power text-danger"></i> Logout</button>
                                    </Link>
                                </div>                                                                    
                            </div>
                        </li>

                    </ul>

                    <ul className="list-inline menu-left mb-0">
                        <li className="float-left">
                            <button className="button-menu-mobile open-left waves-effect" onClick={this.check}>
                                <i className="mdi mdi-menu"></i>
                            </button>
                        </li>
                    </ul>

                </nav>

            </div>
            {
                this.state.toggle ? <HelpModal modal={this.state.toggle} toggle={() =>this.toggleModal(1)} /> :null
            }
            {
                this.state.toggleContact ? <ContactModal modal={this.state.toggleContact} toggle={() =>this.toggleModal(2)} /> :null
            }
            </AUX>
        )
    }
}
export default topchart;
