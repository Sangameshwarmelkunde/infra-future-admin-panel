import React , {useEffect,useState,forwardRef } from 'react';
import { MDBDataTable } from 'mdbreact';
import { CSVLink } from "react-csv";
import AUX from '../../hoc/Aux_';


// ModalConfirm from '../components/Modal/modalConfirm';

const AssessmentTable = React.forwardRef((props,ref) => {
    
    const [columns,setColumns] = useState([]);
    const [rows,setRows] = useState([]);
    const [csvData,setcsvData] = useState([]);


    const  toTitleCase = (str) =>  {
        return str.replace(
            /\w\S*/g,
            function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            }
        );
    }

    const fetchProfile = () =>{
        let  data  = props.data;
        let csvData= [];
        //console.log(data);
        let columns = [];
        let temp_const = Object.keys(data[0]);
        //console.log(temp_const);
        csvData.push(temp_const);
        temp_const.forEach( item => {
            columns.push({
                label:toTitleCase(item),
                field:item,
            })
        })
        for(let i in data){
            csvData.push(Object.values(data[i]));
        }
        //console.log(columns);
        setColumns(columns);
        setRows(data);
        setcsvData(csvData);
        

    }

    useEffect(() =>{
        return fetchProfile();
    }
        ,[]);

    let filename = props.title+Date.now()+".csv";
        
        return(
            <AUX>
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card m-b-20">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-md-6">
                                        <h4 className="mt-0 header-title"><strong>{props.title}</strong></h4>
                                    </div>
                                    <div className="col-md-6 text-right">
                                    
                                        <CSVLink
                                            className="btn btn-primary btn-lg"
                                            filename={filename} 
                                            data={csvData}
                                            asyncOnClick={true}
                                            title="Export assessment result"
                                            ref={ref}
                                        >Export
                                        </CSVLink>
                                    </div>
                                </div>
                                <div style={{overflow:'auto'}}>
                                  <MDBDataTable
                                    
                                      id={"table-to-xls"+props.tableId}
                                      bordered
                                      hover
                                      data={{ columns: columns, rows:rows }}
                                      info={ rows.length > 0 ? true : false}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </AUX>
        );
    
});
  
export default AssessmentTable;