import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Scrollbars } from 'react-custom-scrollbars';
import { connect } from 'react-redux';

import AUX from '../../hoc/Aux_';
import './Leftsidebar.css';
class leftsidebar extends Component {
    
    constructor(props) {
        super(props);
    
        this.state = {
            Tab: '', SubTab: '', MoreTab: '',
        };

    }
    setActiveTab = (tab,subtab,moretab, e) => {
        //console.log(tab)
        //console.log(tab+"---"+subtab+"---"+moretab);
        this.setState({Tab: tab,SubTab: subtab,MoreTab: moretab});
       
    }

    render() {
        let links = (
            <AUX>
                <li className="menu-title">Administrator</li>
                <li>
                    <Link to='/'  className={this.state.Tab === 'profile' ? 'waves-effect active':'waves-effect'} onClick={this.setActiveTab.bind(this, 'profile','','')} >
                    <i className="mdi mdi-clipboard-outline"></i><span> Profile </span></Link>
                </li>
                <li>
                    <Link to='/questions' className={this.state.Tab === 'questions' ? 'waves-effect active':'waves-effect'} onClick={this.setActiveTab.bind(this, 'questions','','')} >
                        <i className="mdi mdi-calendar-check"></i><span> Question </span></Link>
                </li>
                <li>
                    <Link to='/apps' className={this.state.Tab === 'apps' ? 'waves-effect active':'waves-effect'} onClick={this.setActiveTab.bind(this, 'apps','','')} >
                    <i className="mdi mdi-apps"></i><span> Apps </span></Link>
                </li>
                <li>
                    <Link to='/clients' className={this.state.Tab === 'clients' ? 'waves-effect active':'waves-effect'} onClick={this.setActiveTab.bind(this, 'clients','','')} >
                    <i className="mdi mdi-account-network"></i><span> Clients </span></Link>
                </li>
                <li>
                    <Link to='/users' className={this.state.Tab === 'users' ? 'waves-effect active':'waves-effect'} onClick={this.setActiveTab.bind(this, 'users','','')} >
                    <i className="mdi mdi-account"></i><span> Users </span></Link>
                </li>
                <li>
                    <Link to='/sub_admin' className={this.state.Tab === 'sub_admin' ? 'waves-effect active':'waves-effect'} onClick={this.setActiveTab.bind(this, 'sub_admin','','')} >
                    <i className="mdi mdi-voice"></i><span> Admin & Roles </span></Link>
                </li>
                <li>
                    <Link to='/insights' className={this.state.Tab === 'insights' ? 'waves-effect active':'waves-effect'} onClick={this.setActiveTab.bind(this, 'insights','','')} >
                    <i className="mdi mdi-buffer"></i><span> Insights </span></Link>
                </li>
                <li>
                    <Link to='/mail_setting' className={this.state.Tab === 'mail_setting' ? 'waves-effect active':'waves-effect'} onClick={this.setActiveTab.bind(this, 'mail_setting','','')} >
                    <i className="mdi mdi-gmail"></i><span> Mail Setting </span></Link>
                </li>
            </AUX>
        )
        if(parseInt(this.props.accessLevel) === 1){
            links = (
                <AUX>
                    <li className="menu-title">Users</li>
                    <li>
                        <Link to='/'  className={this.state.Tab === 'profile' ? 'waves-effect active':'waves-effect'} onClick={this.setActiveTab.bind(this, 'profile','','')} >
                        <i className="mdi mdi-clipboard-outline"></i><span> Profile </span></Link>
                    </li>
                </AUX>
            );
        }
        else if(parseInt(this.props.accessLevel) === 2){
            links = (
                <AUX>
                    <li className="menu-title">Users</li>
                    <li>
                        <Link to='/' className={this.state.Tab === 'questions' ? 'waves-effect active':'waves-effect'} onClick={this.setActiveTab.bind(this, 'questions','','')} >
                            <i className="mdi mdi-calendar-check"></i><span> Question </span></Link>
                    </li>
                </AUX>
            );

        }
        return (
            <div className="left side-menu">
                <Scrollbars  style={{ height: 800, Color: 'red' }} >
                    <div id="remove-scroll">
                        <div id="sidebar-menu">
                            <ul className="metismenu" id="side-menu">
                                {links}
                            </ul>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                </Scrollbars>
            </div>

        );
    }
}


const mapStateToProps = state => {
    return {
        token: state.auth.token,
        accessLevel:state.auth.accessLevel,

    }
}

export default connect(mapStateToProps)(leftsidebar);