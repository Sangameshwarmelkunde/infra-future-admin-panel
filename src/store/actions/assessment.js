import * as actionTypes from '../action.js';


export const checkAssessment = () =>{
    return dispatch =>{
        const admin_assessment_filter  = JSON.parse(localStorage.getItem('admin_assessment_filter'));
        const admin_cust_filter  = JSON.parse(localStorage.getItem('admin_cust_filter'));
        //console.log(admin_assessment_filter, admin_cust_filter);
        if(admin_assessment_filter || admin_cust_filter)
            dispatch(updateAssessmentFilter(admin_assessment_filter ? admin_assessment_filter : [], admin_cust_filter ? admin_cust_filter : [] ));
    }
}

//update assessment filter
export const updateAssessmentFilter = (assessmentFilter, customerFilter) =>{
    return {
        type:actionTypes.UPDATE_ASSESSMENT_FILTER,
        assessmentFilter:assessmentFilter,
        customerFilter: customerFilter
    }
}

//update local storage for assessment filter
export const assessmentFilter = (assessmentFilter, customerFilter) => {
    //console.log(value);
    return dispatch =>{
        
        localStorage.setItem('admin_assessment_filter',JSON.stringify(assessmentFilter));
        localStorage.setItem('admin_cust_filter',JSON.stringify(customerFilter));

        dispatch(updateAssessmentFilter(assessmentFilter ? assessmentFilter : [], customerFilter ? customerFilter: []));
    }
}