import * as actionTypes from '../action.js';

//   for question mapping
export const updateProfileMapping = (value) =>{
    return {
        type:actionTypes.UPDATE_PROFILE_MAPPING,
        mappingId: value,
    };
};

export const doMapping = (value) => {
    return dispatch =>{
        dispatch(updateProfileMapping(value));
        localStorage.setItem('mappingId',value);
    }
}

// for viewing response
export const updateAppResponse = (value) =>{
    return {
        type:actionTypes.APP_RESPONSE,
        responseId: value,
    };
};

export const setResponse = (value) => {
    return dispatch =>{
        dispatch(updateAppResponse(value));
        localStorage.setItem('responseId',value);
    }
}

// update from local store on refresh

export const checkMapping = () =>{
    return dispatch =>{
        const mappingId = localStorage.getItem('mappingId');
        const responseId = localStorage.getItem('responseId');
        if(mappingId)
            dispatch(updateProfileMapping(mappingId));
        if(responseId)
            dispatch(updateAppResponse(responseId));
    }
}