import * as actionTypes from '../action.js';
import { updateObj } from '../../shared/utility';

const initialState ={
    customerFilter:[],
    assessmentFilter:[]
}

const reducer = (state = initialState, action) =>{
    switch (action.type){
        case actionTypes.UPDATE_ASSESSMENT_FILTER:
            return updateObj(state, {assessmentFilter:action.assessmentFilter, customerFilter: action.customerFilter});
        default:
            return state;
    }
}

export default reducer;