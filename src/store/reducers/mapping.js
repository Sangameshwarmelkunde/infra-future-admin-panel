import * as actionTypes from '../action.js';
import { updateObj } from '../../shared/utility';

const initialState ={
    mappingId:null,
    responseId:null
}

const reducer = (state = initialState, action) =>{
    switch (action.type){
        case actionTypes.UPDATE_PROFILE_MAPPING:
            return updateObj(state, {mappingId:action.mappingId});
        case actionTypes.APP_RESPONSE:
            return updateObj(state, {responseId:action.responseId});
        default:
            return state;
    }
}

export default reducer;