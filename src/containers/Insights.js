import React , {Component } from 'react';
import makeAnimated from "react-select/animated";
import Alert  from '../components/Alert/common';

import AUX from '../hoc/Aux_';
import MySelect from '../components/ReactSelect';
import { handleKeyValueClient,objectToArray, handleKeyValue } from '../shared/utility';

import axios from '../axios-details';
import { connect } from 'react-redux';
import AssessmentTable from '../components/AssessmentTable';
import * as actions from '../store/actions/index.js';

import Donut from './Insight/Donut';
import LineGraph from './Insight/LineGraph';

// import { CSVLink } from "react-csv";
 
let mRefs = []
const animatedComponents = makeAnimated();
class Insights extends Component{
    constructor(props) {
        super(props);
        mRefs = [];
        this.state={
            height:300,
            width:450,
            APPARCHITECTURE:[],
            APPTYPE:[],
            DBTECHNOLOGY:[],
            PROGRAMMINGLANG:[],
            BIZCRITICALITY:[],
            APPROADMAP:[],
            COMPLIANCEREG:[],
            APPCATEGORY:[],
            HWPLATFORM:[],
            OSWEBTIER:[],
            OSDBTIER:[],
            OSAPPTIER:[],
            no_app:0,
            completed:0,
            profile:0,
            loading:true,
            img:'',
            dataF:[],
            toggle:false,
            suitability:[],
            allClients:[],
            selectedClients:[],
            json:{},
            assessmentCount : 0,
            allFilter:[],
            selectedFilter:[],
            notStarted:0,
            buttonMessage:"Refresh Assessment",
            fixedFiler:[
                'APPTYPE','APPARCHITECTURE','DBTECHNOLOGY','PROGRAMMINGLANG','BIZCRITICALITY','APPROADMAP','COMPLIANCEREG','APPCATEGORY','HWPLATFORM','OSWEBTIER','OSDBTIER','OSAPPTIER'
            ],
            alertClass:null,
            error:null,
        }
        this.print = React.createRef();
    }

    componentDidMount(){
        this.fetchClient();
        if(this.props.selectedClients.length > 0){
            this.fetchAPI(this.props.selectedClients);
        }

    }

    fetchClient = () =>{
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        //console.log(config)
        axios.get('/user/userclient', config)
            .then(res => {
                //console.log(res.data)
                let clients = handleKeyValueClient(res.data.data.clients);
                //console.log(clients);
                this.setState({ allClients:clients})
            })
            .catch( err => {
                this.setState({error: "Something went wrong."})
            });    
    }
        
    selectTowChange= (selectedClients) => {
        if(selectedClients && selectedClients.length >0){
            this.fetchAPI(selectedClients);
        }
        else{
            this.setState({dataF:[],no_app:0,completed:0,notStarted:0,profile:0, selectedFilter:[]})
        }
        
        this.props.updateAssessmentFilter(this.props.selectedFilter, selectedClients);
    }

    selectTowChangeFilter= (selectedFilter) => {

        this.props.updateAssessmentFilter(selectedFilter, this.props.selectedClients);
    }

    fetchAPI(selectedClients) {
        this.setState({loading:true});
        const token = {
            token: this.props.token,
            clients: objectToArray(selectedClients),
        };
        //console.log(token);
        axios.post('/insight', token)
            .then(res => {
                let data = res.data.data;
                //console.log(res.data);
                if(res.data.status === true) {
                    let assessmentCount;
                    let assessmentFilter = Object.keys(data.assessResult);
                    let final_filter  = this.state.fixedFiler.concat(assessmentFilter);
                    //console.log(final_filter);
                    assessmentCount = Object.keys(data.assessResult).length;
                    
                    //console.log(data.assessResult);
                    this.setState({
                        loading:false,
                        dataF:data.data,
                        APPARCHITECTURE:data.data.APPARCHITECTURE ? data.data.APPARCHITECTURE : [],
                        APPTYPE:data.data.APPTYPE ? data.data.APPTYPE : [],
                        DBTECHNOLOGY:data.data.DBTECHNOLOGY ? data.data.DBTECHNOLOGY : [],
                        PROGRAMMINGLANG:data.data.PROGRAMMINGLANG ? data.data.PROGRAMMINGLANG : [],
                        
                        BIZCRITICALITY:data.data.BIZCRITICALITY ? data.data.BIZCRITICALITY : [],
                        APPROADMAP:data.data.APPROADMAP ? data.data.APPROADMAP : [],
                        COMPLIANCEREG:data.data.COMPLIANCEREG ? data.data.COMPLIANCEREG : [],
                        APPCATEGORY:data.data.APPCATEGORY ? data.data.APPCATEGORY : [],

                        HWPLATFORM:data.data.HWPLATFORM ? data.data.HWPLATFORM : [],
                        OSWEBTIER:data.data.OSWEBTIER ? data.data.OSWEBTIER : [],
                        OSDBTIER:data.data.OSDBTIER ? data.data.OSDBTIER : [],
                        OSAPPTIER:data.data.OSAPPTIER ? data.data.OSAPPTIER : [],

                        no_app:data.no_app,
                        profile:data.profile,
                        completed:data.completed,
                        notStarted:data.not_started,
                        json: data.assessResult,
                        assessmentCount: assessmentCount,
                        allFilter: handleKeyValue(final_filter),
                        selectedFilter: handleKeyValue(final_filter)
                    })
                    //console.log(data.data);
                    
                }
            })
            .catch( err => {
                this.setState({loading:false})
                // console.log(err)
            });    
    }

    handleAssessmentAPI  =() =>{
        this.setState({loadingMessage:true, buttonMessage:"Loading...",alertMessage: "Please wait. We are fetching assessment data.",error: null })
        const token = {
            token: this.props.token,
            clients: objectToArray(this.props.selectedClients),

        };
        //console.log(token);
        axios.post('/fetchAssessment/', token)
        .then(res => {
            //console.log(res.data);
            if(res.data.status){
                this.setState({loadingMessage:false, buttonMessage:"Refresh Assessment", error: null,})
                this.fetchAPI(this.props.selectedClients);
            }
            else{
                this.setState({loadingMessage:false, buttonMessage:"Refresh Assessment", error: res.data.message, })
            }
            
        })
        .catch( err => {
            this.setState({loadingMessage:false, buttonMessage:"Refresh Assessment", error: "Something went wrong. Please try again"})
         //console.log(err)
        });   
    }


    exportAll = () => {
        mRefs.map((v) => {
            v.exportExcel();
            return v;
        })
    }
  
    render(){
        const { assessmentCount, json } = this.state;
        //console.log(selectedFilter)
        //console.log(this.state.APPARCHITECTURE);
        //const {BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend} = Recharts;
        return(
            <AUX>
                <br />
                <div className="">

                </div>
                <div className="row">
                   
                    <div className="col-md-12">
                        {
                            this.state.loadingMessage ? <Alert classes="alert-danger" msg={this.state.alertMessage} /> : null
                        }
                        {
                            this.state.error ? <Alert classes="alert-danger" msg={this.state.error} /> : null
                        }
                    </div>
                    
                    <div className="col-sm-12 col-lg-1">
                        <div className="page-title-box">
                            <h4 className="page-title">Insights</h4>
                        </div>
                    </div>
                    <div className="col-sm-12 col-lg-8">
                        <div className="page-title-box">
                            <MySelect
                                options={this.state.allClients}
                                isMulti
                                noMin = {true}
                                components={animatedComponents}
                                onChange={this.selectTowChange}
                                allowSelectAll={true}
                                value={this.props.selectedClients}
                            />
                        </div>
                    </div>
                    <div className="col-md-3  col-sm-12">
                        <div className="page-title-box">
                            <button  type="button" title="Import new applications" className="btn btn-primary btn-lg waves-effect" onClick={this.handleAssessmentAPI}>{this.state.buttonMessage}</button>
                        </div>
                    </div>
                    <div className="col-sm-12 mb-2">
                        <MySelect
                            options={this.state.allFilter}
                            isMulti
                            noMin = {true}
                            components={animatedComponents}
                            onChange={this.selectTowChangeFilter}
                            allowSelectAll={true}
                            value={this.props.selectedFilter}
                        />
                    </div> 
                    <div className="col-sm-12 col-lg-4 text-right">
                        {/* <div className="page-title-box">
                            <button  type="button" title="Import new applications" className="btn btn-primary btn-lg waves-effect" onClick={() => this.toggleModal(false)} > Import</button>
                        </div> */}
                    </div>
                </div>
            
                {/* <div className="row">
                    <div className="col-sm-12">
                        <div className="page-title-box">
                            
                        </div>
                    </div> 
                </div> */}
                <div className="row">
                    <div className="col-xl-3 col-md-6">
                        <div className="card mini-stat bg-primary">
                            <div className="card-body mini-stat-img">
                                <div className="mini-stat-icon">
                                    <i className="mdi mdi-cube-outline float-right"></i>
                                </div>
                                <div className="text-white">
                                    <h6 className="text-uppercase mb-3">Total Number Of Apps</h6>
                                    <h4 className="mb-4">{this.state.no_app}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-xl-3 col-md-6">
                        <div className="card mini-stat bg-primary">
                            <div className="card-body mini-stat-img">
                                <div className="mini-stat-icon">
                                    <i className="mdi mdi-buffer float-right"></i>
                                </div>
                                <div className="text-white">
                                    <h6 className="text-uppercase mb-3">Completed Surveys</h6>
                                    <h4 className="mb-4">{this.state.completed}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-xl-3 col-md-6">
                        <div className="card mini-stat bg-primary">
                            <div className="card-body mini-stat-img">
                                <div className="mini-stat-icon">
                                    <i className="mdi mdi-tag-text-outline float-right"></i>
                                </div>
                                <div className="text-white">
                                    <h6 className="text-uppercase mb-3">Ongoing Surveys</h6>
                                    <h4 className="mb-4">{this.state.no_app - this.state.notStarted - this.state.completed}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-xl-3 col-md-6">
                        <div className="card mini-stat bg-primary">
                            <div className="card-body mini-stat-img">
                                <div className="mini-stat-icon">
                                <i className="mdi mdi-briefcase-check float-right"></i>
                                </div>
                                <div className="text-white">
                                    <h6 className="text-uppercase mb-3">New Surveys</h6>
                                    <h4 className="mb-4">{this.state.notStarted}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                {
                    Object.keys(this.state.dataF).length > 0 ?   
                
                (<>
                <div className="row" >
                    { this.props.selectedFilter.some(q => q.label === "APPTYPE") ?
                        <Donut title="APPTYPE" loading={this.state.loading} data={this.state.APPTYPE} />
                    : null}
                    { this.props.selectedFilter.some(q => q.label === "APPARCHITECTURE") ?
                        <Donut title="APPARCHITECTURE" loading={this.state.loading} data={this.state.APPARCHITECTURE} />
                    : null}
                    { this.props.selectedFilter.some(q => q.label === "BIZCRITICALITY") ?
                        <Donut title="BIZCRITICALITY" loading={this.state.loading} data={this.state.BIZCRITICALITY} />
                    : null}
                </div>  
                <div className="row" >
                    { this.props.selectedFilter.some(q => q.label === "DBTECHNOLOGY") ?
                        <LineGraph title="DBTECHNOLOGY" loading={this.state.loading} data={this.state.DBTECHNOLOGY} />
                    : null}
                    { this.props.selectedFilter.some(q => q.label === "PROGRAMMINGLANG") ?
                        <LineGraph title="PROGRAMMINGLANG" loading={this.state.loading} data={this.state.PROGRAMMINGLANG} />
                    : null}
                    { this.props.selectedFilter.some(q => q.label === "APPROADMAP") ?
                        <LineGraph title="APPROADMAP" loading={this.state.loading} data={this.state.APPROADMAP} />
                    : null}
                    { this.props.selectedFilter.some(q => q.label === "COMPLIANCEREG") ?
                        <LineGraph title="COMPLIANCEREG" loading={this.state.loading} data={this.state.COMPLIANCEREG} />
                    : null}
                    { this.props.selectedFilter.some(q => q.label === "APPCATEGORY") ?
                        <LineGraph title="APPCATEGORY" loading={this.state.loading} data={this.state.APPCATEGORY} />
                    : null}
                    { this.props.selectedFilter.some(q => q.label === "HWPLATFORM") ?
                        <LineGraph title="HWPLATFORM" loading={this.state.loading} data={this.state.HWPLATFORM} />
                    : null}
                    { this.props.selectedFilter.some(q => q.label === "OSWEBTIER") ?
                        <LineGraph title="OSWEBTIER" loading={this.state.loading} data={this.state.OSWEBTIER} />
                    : null}
                    { this.props.selectedFilter.some(q => q.label === "OSDBTIER") ?
                        <LineGraph title="OSDBTIER" loading={this.state.loading} data={this.state.OSDBTIER} />
                    : null}
                    { this.props.selectedFilter.some(q => q.label === "OSAPPTIER") ?
                        <LineGraph title="OSAPPTIER" loading={this.state.loading} data={this.state.OSAPPTIER} />
                    : null}
                </div>  
                
                </>) : null }
                <br />
                {/* <ReactHTMLTableToExcel
                    id="test-table-xls-button"
                    className="download-table-xls-button"
                    table={["table-to-xls0","table-to-xls1"]}
                    filename="tablexls"
                    sheet="tablexls"
                    buttonText="Download as XLS"/>
                
                */}
               
                <div className="row">
                    <div className="col-md-12 text-right">
                        <button className="btn btn-lg btn-primary" onClick={this.exportAll}>Export All</button>
                    </div>
                </div>
                <br />
                {
                    assessmentCount > 0 ? Object.keys(json).map((q, i) =>{
                        //console.log(json[q])
                        return ( this.props.selectedFilter.some(t => t.label === q) ?   <AssessmentTable key={i} ref={(ref) => ref && mRefs.push(ref)} tableId={i} title={q} data={json[q]} /> : null)
                    }) : null
                }
                {/* <br />
                <div className="row" style={{overflow: 'auto', backgroundColor:'white'}}>
                    <table className="table table-bordered mb-0">
                        <thead>
                            <tr>
                                <th>Apps</th>
                                <th>Technical Fitness</th>
                                <th>Business Value</th>
                                <th>Risk</th>
                                <th>Global AHP Score</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.suitability.map((s,i) =>{
                                return (
                                    <tr key={i}>
                                        <th scope="row">{s.app_name}</th>
                                        <td>{s.businessValue}</td>
                                        <td>{s.globalAHPScore}</td> 
                                        <td>{s.risk}</td> 
                                        <td>{s.techinicalFitness}</td> 
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
                <br /> */}
            
            {/* {
                this.state.toggle
                ? <UploadModal modal={this.state.toggle} toggle={this.toggleModal} title="Upload Insights" />
                : false
            } */}
            </AUX>
        );
    }
}

const mapStateToProps = state =>{
    return {
        token : state.auth.token,
        selectedFilter : state.assessment.assessmentFilter,
        selectedClients: state.assessment.customerFilter
    }
}

const mapDispatchToProps = dispatch =>{
    return {
        updateAssessmentFilter : (selectedFilter, selectedClients) => dispatch(actions.assessmentFilter(selectedFilter, selectedClients))
    }
    
}

export default connect(mapStateToProps, mapDispatchToProps)(Insights);
  