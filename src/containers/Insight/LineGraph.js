import React from 'react';
import Simpleline from '../Chartstypes/Simpleline';
import SimpleSpinner from '../../components/Spinner/simpleSpinner';


const LineGraph = (props) =>{
    return (
        <div className="col-xl-4">
            <div className="card m-b-20">
                <div className="card-body">
                    <h4 className="mt-0 header-title">{props.title}</h4>
                    <div id="morris-bar-stacked" className="dashboard-charts morris-charts">
                    {
                        props.loading ? <SimpleSpinner /> :<Simpleline side="small" datas={props.data} name={props.title} />
                    }
                    </div>
                    
                </div>
            </div>
        </div>
    )
}

export default LineGraph;