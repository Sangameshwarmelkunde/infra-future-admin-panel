import React , {Component } from 'react';
import {Doughnut} from 'react-chartjs-2';
 
  class Donut extends Component{
	state={
		labels: [
			'one',
			'two',
			'three'
		],
		datasets: [{
			data: [300, 500 , 200],
			backgroundColor: [
				'#7a6fbe',
				'#29bbe3',
				'#f4c63d',
				'#58db83',
				'#65727f',
			],
			hoverBackgroundColor: [
				'#7a6fbe',
				'#29bbe3',
				'#f4c63d',
				'#58db83',
				'#65727f',
			]
		}]
		
	} 

	componentDidMount(){
		let levels=[];
		let data =[];
		// console.log(this.props.datas);
		Object.keys(this.props.datas).forEach(item => {
			levels.push(item);
			data.push(this.props.datas[item]);
		})
		let {datasets} = this.state;
		datasets=[{
			...datasets,
			data
		}]

		this.setState({labels: levels, datasets: datasets});
	}
			 
	render() {
		//console.log(this.state);
		return (
			<div>
			<Doughnut height={265} data={this.state} />
			</div>
		);
		}
	}

 
export default Donut;