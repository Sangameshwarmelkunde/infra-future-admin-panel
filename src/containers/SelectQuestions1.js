import React , {Component } from 'react';
import AUX from '../hoc/Aux_';
import { connect } from 'react-redux';
import makeAnimated from "react-select/animated";
import { Link, Redirect } from 'react-router-dom';
import { Prompt } from 'react-router-dom';
import { handleKeyValue,objectToArray } from '../shared/utility';


import axios from '../axios-details';
import MySelect from '../components/ReactSelect';
import './response.css';
import Alert  from '../components/Alert/common';
import Spinner from '../components/Spinner/simpleSpinner';

import './response.css';
let selectedQuestion = new Set();
const animatedComponents = makeAnimated();

class SelectQuestions extends Component{

    constructor(props){
        super(props);
        this.state = {
            profileName: '',
            category: [],
            questions: [],

            loading:false,
            message:null,
            error:null,
            alQs:['1'],
            checked:false,
            isSubmitted:false,
            formIsHalfFilledOut:false,

            allCriteria:[],
            selectedCriteria:[],
            allLevels: [],
            selectedLevel:[],
            allTopics:[],
            selectedTopics:[],
        }
    }

    fetchAPI() {
        this.setState({loading:true, message:null, error:null});
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        //console.log(config);
        axios.get('/profile/map/'+this.props.mappingId, config)
            .then(res => {
                //console.log(res.data.data);
                selectedQuestion = new Set();
                res.data.data.questions.map(q =>{
                    selectedQuestion.add(q);
                })
                //console.log(selectedQuestion);
                if(res.data.status) {
                    let criteria = handleKeyValue(res.data.data.criteria);
                    let levels = handleKeyValue(res.data.data.levels);
                    let topics = handleKeyValue(res.data.data.topics);
                    this.setState({
                        profileName: res.data.data.profile_name,
                        questions: res.data.data.data_obj,
                        category: res.data.data.categories,
                        loading:false,

                        selectedCriteria: criteria,
                        allCriteria: criteria,
                        selectedLevel: levels,
                        allLevels : levels,
                        allTopics: topics,
                        selectedTopics: topics,
                    })
                }
                else {
                    this.setState({loading:false, error:res.data.message});
                }
            })
            .catch( err => {
                this.setState({loading:false, error:"Something went wrong. Please try again."});
            });    
    }

    componentDidMount() {
        this.fetchAPI();
    }

    checkAllChech(selectedCat){
        //console.log(selectedQuestion);
        let {questions} = this.state
        let question = questions[selectedCat];
        var ret = false;
        for(var i =0; i<question.length; i++){
            ret =  selectedQuestion.has(question[i].quest_id.toString()) ;
            if(ret === false){
                return false;
            }
        }
        return true;
    }
   

    handleCheckbox = (event, quest_id) =>{
        //console.log(event.target)
        if(event.target.checked){
            selectedQuestion.add(quest_id.toString());
        }
        else{
            selectedQuestion.delete(event.target.value)
        }
        this.setState({formIsHalfFilledOut:true});
    }

    handleCheckboxAll = (event, type) =>{
        let {questions} = this.state
        let question = questions[type];

        if(event.target.checked){
            question.map((q,i)=>{
                selectedQuestion.add(q.quest_id.toString());
            })
            this.setState({checked:true});
        }
        else{
            question.map((q,i)=>{
                selectedQuestion.delete(q.quest_id.toString());
            })
        }
        // this.checkAllChech(selectedQuestion);
        this.setState({formIsHalfFilledOut:true});
    }

    selectTowChangeCriteria= (selectedCriteria) => {
        this.setState({ selectedCriteria:selectedCriteria ?  selectedCriteria : [] });
    }

    selectTowChangeLevel= (selectedLevel) => {
        this.setState({ selectedLevel:selectedLevel ? selectedLevel : [] });
    }

    selectTowChangeTopics= (selectedTopics) => {
        this.setState({ selectedTopics:selectedTopics ? selectedTopics : [] });
    }


    handleQuestionsSelected = (event) =>{
        event.preventDefault();
        //console.log(this.state.selectedQuestion);
        this.setState({loading:true, message:null, error:null, formIsHalfFilledOut:false});
        const data = {
            token: this.props.token,
            profile_id: this.props.mappingId,
            questions: [...selectedQuestion],
        }
        //console.log(data);
        axios.post('/profile/map/', data)
            .then(res => {
                //console.log(res.data.data);
                if(res.data.status) {
                    this.setState({loading:false, isSubmitted:true, message:res.data.message});
                }
                else {
                    this.setState({loading:false, isSubmitted:true, error:res.data.message});
                }
            })
            .catch( err => {
                this.setState({loading:false, isSubmitted:true, error:"Something went wrong. Please try again."});
            });    
    }
    render(){
        let i = 0, j = 0;
        let id, href, anwser_key, cname, cname1, criteria, levels, topics;
        let { selectedCriteria, selectedLevel, selectedTopics } = this.state;
        criteria=objectToArray(selectedCriteria);
        levels = objectToArray(selectedLevel);
        topics = objectToArray(selectedTopics);
        return(
            <AUX>
                <Prompt
                    when={this.state.formIsHalfFilledOut}
                    message="Are you sure you want to leave?"
                />
                {
                    this.state.isSubmitted? <Redirect to="/" /> : null
                
                }
                <div className="row">
                    <div className="col-sm-12">
                        <div className="page-title-box">
                        <h4 className="page-title">Profile</h4>
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                                <li className="breadcrumb-item"><Link to="/">Profile</Link></li>
                                <li className="breadcrumb-item active">Select Question</li>
                            </ol>
                            {/* <Tinycharts /> */}
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-12 col-lg-6">
                        <div className="page-title-box">
                            
                            <label>Level</label>
                            <MySelect
                                value={this.state.selectedLevel}
                                onChange={this.selectTowChangeLevel}
                                options={this.state.allLevels}
                                components={animatedComponents}
                                allowSelectAll={true}
                                isMulti ={true}
                            />
                            </div>
                    </div>
                    <div className="col-sm-12 col-lg-6">
                        <div className="page-title-box">
                            <label>Criteria</label>
                            <MySelect
                                value={this.state.selectedCriteria}
                                onChange={this.selectTowChangeCriteria}
                                options={this.state.allCriteria}
                                components={animatedComponents}
                                allowSelectAll={true}
                                isMulti ={true}
                            />
                            </div>
                    </div>
                    <div className="col-sm-12 col-lg-12">
                        <div className="page-title-box">
                            <label>Topics</label>
                            <MySelect
                                value={this.state.selectedTopics}
                                components={animatedComponents}
                                onChange={this.selectTowChangeTopics}
                                options={this.state.allTopics}
                                allowSelectAll={true}
                                isMulti ={true}
                            />
                            </div>
                    </div>
                    
                </div>
                        
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card m-b-20">
                        {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} />: null}
                        {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} />: null}
                        {this.state.loading ?  <Spinner /> : null }
                            <div className="card-body">
                                <form onSubmit={(event) => this.submitHandler(event)}>
                                <h4 className="mt-0 header-title"> {this.state.profileName}
                                <button type="button" className="btn btn-primary" style={{float: "right"}} onClick={(e) => this.handleQuestionsSelected(e)} >Submit</button>
                                </h4>
                                <ul className="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                                
                                {   this.state.category.map((item, index) => {

                                    id = "tab_" + i;
                                    href = "#qs_tab_" + i++;
                                    cname = "nav-link "+(index ===0 ? "active" : null)

                                    return (<li className="nav-item"  key={index} >
                                        <a className={cname}  id={id} data-toggle="tab" href={href} role="tab">{item}</a>
                                    </li>)
                                    
                                })}

                                </ul>

                                <div className="tab-content">
                                {
                                    this.state.category.map((item, index) => {
                                        id = "qs_tab_" + j++;
                                        cname1 = "tab-pane p-3 "+(index === 0 ? "active" : null)

                                    return (
                                    <div key={index} className={cname1} id={id} role="tabpanel" style={{overflow: 'auto'}}>
                                        <table className="table table-bordered mb-0">
                                            <thead>
                                                <tr>
                                                <th>                                                      <label className="container">
                                                        <input
                                                            type="checkbox" 
                                                            value={this.state.alQs || ''}
                                                            onChange={(event) => this.handleCheckboxAll(event, item)}
                                                            checked={this.checkAllChech(item)}
                                                            />
                                                        <span className="checkmark"></span>
                                                    </label>
                                                    <br/>
                                                </th>
                                                <th>Questions</th>
                                                <th>Description</th>
                                                <th>Response Type</th>
                                                <th>Responses</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {
                                                this.state.questions[item].map((qs, index) => {
                                                    anwser_key = qs.response_json;
                                                    //console.log(qs.quest_id);
                                                    //answers = qs.answer;
                                                    // allQuestions.add(qs.quest_id.toString());
                                                    // check filter
                                                    if((criteria && criteria.includes(qs.ahp_criteria_lev_1)) && (levels && levels.includes(qs.levels)) &&  (topics && topics.some(r=> qs.topics.indexOf(r) >= 0) )){

                                                    return (<tr key={index}>
                                                        <th scope="row">
                                                            <label className="container">
                                                                <input 
                                                                    type="checkbox" 
                                                                    value={qs.quest_id} 
                                                                    onChange={(event) => this.handleCheckbox(event, qs.quest_id)}
                                                                    checked={ selectedQuestion ? selectedQuestion.has(qs.quest_id.toString()) ? true : false : false}
                                                                    />
                                                                <span className="checkmark"></span>
                                                            </label>
                                                        </th>
                                                        <th>{qs.question}</th>
                                                        <td>{qs.description}</td>
                                                        <td>{qs.response_type}</td>
                                                        <td>

                                                        {anwser_key.map((key, index) => {
                                                            // val = key.position

                                                            if(qs.response_type === 'mcq-single')
                                                                return (
                                                                <label key={index} className="container">{key.choice}</label>
                                                                )

                                                            else
                                                                return (
                                                                <label key={index} className="container">{key.choice}</label>
                                                                )
                                                        })}

                                                        </td>
                                                    </tr>);
                                                    }
                                                })
                                                
                                            }
                                            </tbody>
                                        </table>
                                    </div>)
                                })}

                                </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
                
            </AUX>
            
        );
    }
    
}
const mapStateToProps = state =>{
    return {
        token : state.auth.token,
        mappingId : state.mapping.mappingId
    }
}

export default connect(mapStateToProps)(SelectQuestions);

