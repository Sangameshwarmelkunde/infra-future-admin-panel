import React , {Component } from 'react';
import { connect } from 'react-redux';

import AUX from '../hoc/Aux_';
import axios from '../axios-details';
import MySelect from '../components/ReactSelect';
import { handleKeyValueMail,objectToArray } from '../shared/utility';

import Spinner from '../components/Spinner/simpleSpinner';
import Alert from '../components/Alert/common.js';

class MailSetting extends Component{

    constructor(props){
        super(props);
        this.state = {
            
            loading: false,
            error:null,
            message:null,

            selectedEndpoint:[],
            allEndpoints:[],
            selectedMailTo:[],
            allMailTo:[],
        }
    }

    fetchSetting = ()  =>{
        this.setState({ loading:true });
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        axios.get('/mail_setting/',config)
            .then(res => {
                
                //console.log(res.data.data);
                const { data } = res.data;
                if(res.data.status){
                    //console.log(data);
                    this.setState({
                        selectedEndpoint: handleKeyValueMail(data.selected_endpoint),
                        allEndpoints: handleKeyValueMail(data.all_endpoint),
                        selectedMailTo: handleKeyValueMail(data.selected_mail),
                        allMailTo: handleKeyValueMail(data.all_mail),
                    })
                }
                this.setState({loading:false})
                
            })
            .catch( err => {
                this.setState({loading:false})
            }
                
            );    
    }

    componentDidMount() {
        this.fetchSetting();
    }

    selectTowChange= (e,i) => {
        this.setState({ [i]:e });
    }

    // delete profile
    submitHandler = (e) =>{
        e.preventDefault();
        this.setState({loading:false,error:null,message:null});
        const data = {
            token: this.props.token,
            mail_endpoint: objectToArray(this.state.selectedEndpoint),
            mail_to: objectToArray(this.state.selectedMailTo),
        }
        //console.log(data);
        axios.post('/mail_setting/', data)
        .then(res => {
            //console.log(res.data);
            if(res.data.status){
                this.setState({loading:false,message: res.data.message})
            }
            else{
                this.setState({loading:false,error: res.data.message})
            }
        })
        .catch(err => {
            this.setState({loading:false,error: "Something went wrong. Please try again later."});
            
        })
    }


    render(){
        //console.log(this.state);
        const { selectedEndpoint, allEndpoints, selectedMailTo, allMailTo } = this.state;
        return(
            <AUX>
                <div className="row">
                    <div className="col-sm-12">
                        <div className="page-title-box">
                        <h4 className="page-title">Mail Setting</h4>
        
                        </div>
                    </div>
                </div>
                        
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card m-b-20" style={{marginBottom:'20%'}}>
                            <div className="card-body" >
                                {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} />: null}
                                {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} />: null}
                                {this.state.loading ?  <AUX><Spinner /><br /></AUX> : null }
                                {/* <h4 className="mt-0 header-title text-left">
                                    Mail Setting
                                </h4> */}
                                <form onSubmit={this.submitHandler}>
                                    <div className="row">
                                        <div className="col-sm-12 col-lg-6">
                                            <div className="page-title-box">
                                                <label>Mail Endpoint</label>
                                                <MySelect
                                                    value={selectedEndpoint}
                                                    onChange={(e) => this.selectTowChange(e, "selectedEndpoint")}
                                                    options={allEndpoints}
                                                    allowSelectAll={true}
                                                    isMulti ={true}
                                                    noMin={true}
                                                />
                                                </div>
                                        </div>
                                        <div className="col-sm-12 col-lg-6">
                                            <div className="page-title-box">
                                                <label>Mail To</label>
                                                <MySelect
                                                    value={selectedMailTo}
                                                    onChange={(e) => this.selectTowChange(e, "selectedMailTo")}
                                                    options={allMailTo}
                                                    allowSelectAll={true}
                                                    isMulti ={true}
                                                    noMin={true}
                                                />
                                                </div>
                                        </div>
                                        <div className="col-sm-12 text-center">
                                            <button type="submit" className="btn btn-primary waves-effect waves-light">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
            </AUX>
        );
    }
}

const mapStateToProps = state => {
    return {
        token:state.auth.token
    }
}

export default connect(mapStateToProps)(MailSetting);
