
import React, { Component } from 'react';
import AUX from '../../hoc/Aux_';
import { connect } from 'react-redux';
import * as actionTypes from '../../store/action';
import { Link } from 'react-router-dom';
class Register extends Component {
      
    componentDidMount() 
    {
        if(this.props.loginpage === false)
        {
          this.props.UpdateLogin();
        }
        
        window.onpopstate  = (e) => {
          this.props.UpdateLoginAgain();
        }
    }

    render() {
   
        return (
            <AUX>
                <nav className="navbar-custom" style={{marginLeft: '0px'}}>
                    <ul className="navbar-left d-flex list-inline float-left mb-0">
                        <li>
                            <Link to="/" className="logo logo-admin mt-4 mb-2">
                                <div class="pt-3 pl-3">
                                    <img src="assets/images/logo-sm.png" alt="" height="22" /><span className="text-muted"> AppTransition</span>
                                </div>
                                
                            </Link>
                        </li>
                    </ul>
                    <ul className="navbar-right d-flex list-inline float-right pt-3 pl-3">
                        <li>
                            <Link to="/register" style={{border: "1px solid #1e61cd"}} className="btn btn-light w-md waves-effect waves-light" >Login</Link>
                        </li>
                    </ul>
                    <ul className="navbar-right d-flex list-inline float-right pt-4 pl-3">
                        <li className="dropdown notification-list d-none d-sm-block">
                        Already have an account?
                        </li>
                    </ul>
                </nav>
                <div className="wrapper-page">
                <div className="card">
                <div className="card-body">

                    <div className="p-3">
                        <h4 className="text-muted font-18 m-b-5 text-center">Your email has been verified. Please click here to <Link to='/login' className="btn btn-primary w-md waves-effect waves-light">Login</Link></h4>                       
                    </div>
                </div>
            </div>
        </div>
            </AUX>
        );
    }
}

const mapStatetoProps = state => {
    return {
        loginpage: state.ui_red.loginpage
    };
}

const mapDispatchtoProps = dispatch => {
    return {
        UpdateLogin: () => dispatch({ type: actionTypes.LOGINPAGE, value: true }),
        UpdateLoginAgain: () => dispatch({ type: actionTypes.LOGINPAGE, value: false })
    };
}

export default connect(mapStatetoProps, mapDispatchtoProps)(Register);

