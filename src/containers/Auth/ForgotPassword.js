
import React, { Component } from 'react';
import AUX from '../../hoc/Aux_';
import axios from '../../axios-details';
import { specialCharacters } from '../../Utils/specialCharater';

import Alert from '../../components/Alert/common';
import SimpleSpinner from '../../components/Spinner/simpleSpinner';
import CommonNav from '../../components/CommonNav';

class ForgotPassword extends Component {
    constructor(props){
        super(props);
        this.state = {
            data:[],
            password: '',
            confirmed: '',
            getData:false,
            loading:false,
            error: null,
            message:null,
            incorrectPassword:null,
        }
    }

    checkToken = ()  =>{
        this.setState({ loading:true, message:null, error:null});
        let params = new URLSearchParams(this.props.location.search);
        axios.get('/forgot/'+params.get('token'))
            .then(res => {
                //console.log(res.data);
                if(res.data.status)
                    this.setState({ loading:false, data:res.data.data.user, getData:true });
                else    
                    this.setState({ loading:false, error:"Invalid reset link. Please try again"})
            })
            .catch( err => {
                this.setState({loading:false, data: "The verification link may be deactivated. Please try again"})
            }
                
            );     
      }

    componentDidMount(){
        this.checkToken();
    }

    submitHandler = (event) => {
        event.preventDefault();
        //console.log(this.state.password.length);
        if(this.state.password.length >= 6 && specialCharacters(this.state.password)){
            this.setState({loading:true, error:null, message:null, incorrectPassword:false});
            if(this.state.password === this.state.confirmed ){
                const authData = {
                    admin_id: this.state.data.admin_id,
                    password: this.state.password,
                }
                //console.log(authData);
                axios.post('/forgot/reset',authData)
                .then(res => {
                    //console.log(res.data);
                    if(res.data.status){
                        this.setState({loading:false, message: "Password has been updated. Please login with new password"});
                    }
                    else{
                        this.setState({loading:false, error: res.data.message}); 
                    }
                    //console.log(res.data);
                }).catch(err =>{
                    //console.log(err);
                    this.setState({loading:false, error: "Something went wrong. Please try again."});
                });
            }
            else{
                this.setState({loading:false, error: "Both password are not same"});
            }
        }
        else{
            this.setState({incorrectPassword: true})
        }
        
    }
    
    inputChangeHandler = (event, controlName) =>{
        //console.log(controlName);
        this.setState({[controlName]:event.target.value});
    }

    render() {
        let body = (
            <form className="form-horizontal m-t-30" onSubmit={this.submitHandler}>
                <div className="form-group">
                    <label htmlFor="email">Email ID</label>
                    <input 
                        disabled
                        type="text" 
                        className="form-control" 
                        value={this.state.data.username}
                         />
                </div>

                <div className="form-group">
                    <label htmlFor="userpassword">Password</label>
                    <input 
                        required
                        type="password" 
                        className="form-control" 
                        onChange={(event) => this.inputChangeHandler(event,"password" )}
                        placeholder="Enter Password" />
                    <span className={this.state.incorrectPassword ? "passwordStyle" : null}>At least 6 characters having one special character.</span>
                </div>

                <div className="form-group">
                    <label htmlFor="userpassword">Confirm Password</label>
                    <input 
                        required
                        type="password" 
                        className="form-control" 
                        onChange={(event) => this.inputChangeHandler(event,"confirmed" )}
                        placeholder="Confirm password" />
                </div>

                <div className="form-group row m-t-20">
                    <div className="col-12 text-center">
                        <button className="btn btn-primary w-md waves-effect waves-light" type="submit">Submit</button>
                    </div>
                </div>
                <div className="form-control text-right" style={{border: 0}}>
                    <p><a href="/" className="text-primary"> Click here to login.  </a> </p>
                </div>
            </form>
        )
        return (
            <AUX>
                <CommonNav />
                <div className="wrapper-page">
                    <div className="card">
                        <div className="card-body">

                            <div className="p-3">
                                <h4 className="text-muted font-18 m-b-5 text-center">Reset Password</h4>
                                {this.state.error ? <Alert msg={this.state.error} classes="alert-danger" /> : null }
                                {this.state.message ? <Alert msg={this.state.message} classes="alert-success" /> : null }
                                {this.state.loading ?  <SimpleSpinner /> : null }
                                {this.state.getData ? body : null }
                            </div>
                        </div>
                    </div>
                </div>
            </AUX>
        );
    }
}

export default ForgotPassword;

