import React , {Component } from 'react';
import AUX from '../hoc/Aux_';
import { connect } from 'react-redux';
import { CSVLink } from "react-csv";
import { Link } from 'react-router-dom';
import { Progress } from 'reactstrap';
import axios from '../axios-details';

import './response.css';
import Spinner from '../components/Spinner/simpleSpinner';


const header = ['App Name', 'Question Tag', 'Question', 'Client Response'];


class Response extends Component{


    constructor(props){
        super(props);
        this.state = {
            appName: '',
            answered: '',
            ansCount: 0,
            totalCount: 0,
            category: [],
            questions: [],
            loading:false,
            message:null,
            error:null,
            checking: false,
            csvData: [],
        }
        this.csvs = React.createRef();
    }


    fetchAPI() {
        this.setState({loading:true, message:null, error:null});
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        
        axios.get('/apps/'+this.props.responseId,config)
            .then(response => {
                
                let {data:res} = response
                //console.log(response.data);
                if(res.status === true) {
                    this.setState({
                        appName: res.data.app_name,
                        answered: res.data.answered,
                        ansCount: res.data.ans_count,
                        totalCount: res.data.total_qs,
                        questions: res.data.data_obj,
                        category: res.data.categories,
                        loading:false,
                    })
                    this.exportCSV(res.data.categories,res.data.data_obj);
                }

                else {
                    this.setState({loading:false, error:response.data.message});
                }
                
            })
            .catch( err => {
                this.setState({loading:false, error:"Something went wrong. Please try again."});
            });    
    }

    componentDidMount() {
        this.fetchAPI();
    }
    
    exportCSV = (category, questions) =>{
        let answer_key, answers, val;
        let {csvData} = this.state;
        csvData = [];
        csvData.push(header);
        
        category.map(cat =>{
            questions[cat].map((quest, index) => {
                answer_key = quest.response_json;
                answers = quest.answer;

                quest.question = quest.question.replace(/"/g, '""');

                let arr = [];
                answer_key.map((key, index) => {
                    val = key.position;
                    if(answers && answers.includes(val.toString())){
                        arr.push(key.choice.trim())
                        
                    }
                })
                csvData.push([this.state.appName, quest.quest_tag, quest.question, arr ? arr : '']);
            })
        })
        this.setState({csvData})
    }
    
    render(){
        
        let filename = Date.now()+".csv";
        let i = 0, j = 0;
        let id, href, val, anwser_key, cname, cname1, answers;
        let progressBar  = parseInt(this.state.ansCount)/parseInt(this.state.totalCount)*100;
        //console.log(this.state.csvData);
        return(
            <AUX>
                <div className="row">
                    <div className="col-sm-12">
                        <div className="page-title-box">
                        <h4 className="page-title">Response</h4>
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                                <li className="breadcrumb-item"><Link to="/apps">Apps</Link></li>
                                <li className="breadcrumb-item active">Response</li>
                            </ol>
                            {/* <Tinycharts /> */}
                        </div>
                    </div>
                </div>
                        
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card m-b-20">
                            {this.state.loading ?  <Spinner /> : null }
                            <div className="card-body">
                                
                                <h4 className="mt-0 header-title">Assessment | {this.state.appName}
                                <div className="text-right"> 
                                   
                                        <CSVLink

                                            className="btn btn-success mb-3"
                                            filename={filename} 
                                            data={this.state.csvData}
                                            asyncOnClick={true}
                                        >
                                            Export
                                        </CSVLink>
                                    
                                </div>
                                </h4>
                                <ul className="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                                
                                {   this.state.category.map((item, index) => {

                                    id = "tab_" + i;
                                    href = "#qs_tab_" + i++;
                                    cname = "nav-link "+(index ===0 ? "active" : null)

                                    return (<li className="nav-item"  key={index} >
                                        <a className={cname}  id={id} data-toggle="tab" href={href} role="tab">{item}</a>
                                    </li>)
                                    
                                })}

                                </ul>

                                <div className="tab-content">
                                    <Progress color="success" value={progressBar} />

                                {
                                    this.state.category.map((item, index) => {

                                        id = "qs_tab_" + j++;
                                        cname1 = "tab-pane p-3 "+(index === 0 ? "active" : null)

                                    return (
                                    <div key={index} className={cname1} id={id} role="tabpanel" style={{overflow: 'auto'}}>
                                        <table className="table table-bordered mb-0">
                                            <thead>
                                            <tr>
                                                <th>QUESTIONS</th>
                                                <th>DESCRIPTION</th>
                                                <th>RESPONSES</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            {
                                                this.state.questions[item].map((qs, index) => {
                                                    anwser_key = qs.response_json;
                                                    answers = qs.answer;
                                                    return (<tr key={index}>
                                                        <th scope="row">{qs.question}</th>
                                                        <td>{qs.description}</td>
                                                        <td>

                                                        {anwser_key.map((key, index) => {
                                                            val = key.position

                                                            if(qs.response_type === 'mcq-single')
                                                                return (
                                                                <label key={index} className="container">{key.choice}
                                                                    <input 
                                                                        
                                                                        type="radio" 
                                                                        value={val} 
                                                                        name={qs.quest_id}
                                                                        disabled
                                                                        checked={ answers ? (answers.includes(val.toString()) ? "checked" : null) : null} />
                                                                    <span className="radiomark"></span>
                                                                </label>
                                                                )

                                                            else
                                                                return (
                                                                <label key={index} className="container">{key.choice}
                                                                    <input 
                                                                        type="checkbox" 
                                                                        value={val} 
                                                                        name={qs.quest_id}
                                                                        disabled
                                                                        checked={ answers ? answers.includes(val.toString()) ? "checked" : null : null}
                                                                        />
                                                                    <span className="checkmark"></span>
                                                                </label>
                                                                )
                                                        })}

                                                        </td>
                                                    </tr>);
                                                })
                                            }


                                            </tbody>
                                        </table>
                                    </div>)
                                })}

                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </AUX>
        );
    }
}

const mapStateToProps = state =>{
    return {
        token : state.auth.token,
        responseId : state.mapping.responseId
    }
}

export default connect(mapStateToProps)(Response);
