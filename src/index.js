import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter } from 'react-router-dom';

import { createStore ,combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';





import  UIreducer from './store/reducers/reducer';
import  Tinyreducer from './store/reducers/TinychartReducer';
import AuthReducer from './store/reducers/auth';
import MappingReducer from './store/reducers/mapping';
import assessment from './store/reducers/assessment';
import { Provider } from 'react-redux';

//cssTransform
import './public/plugins/magnific-popup/magnific-popup.css';
import './public/fullcalendar/css/fullcalendar.min.css';
import './public/assets/css/bootstrap.min.css';
import './public/assets/css/metismenu.min.css';
import './public/assets/css/icons.css';
import './public/assets/css/style.css';

import 'jquery/dist/jquery.min.js'
import 'bootstrap/dist/js/bootstrap.min.js'

// import './public/assets/js/assets/js/jquery.min.js';
// import './public/assets/js/bootstrap.bundle.min.js';
// import './public/assets/js/metisMenu.min.js';
// import './public/assets/js/jquery.slimscroll.js';
// import './public/assets/js/waves.min.js';
//\import './public/assets/js/app.js';
// import './public/plugins/jquery-sparkline/jquery.sparkline.min.js';

const rootReducer = combineReducers({
    ui_red:UIreducer,
    tiny_red:Tinyreducer,
    auth: AuthReducer,
    mapping: MappingReducer,
    assessment: assessment,
});

const composeEnhancers = compose;

const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(thunk)
  ));

const app = (
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));

serviceWorker.unregister();
