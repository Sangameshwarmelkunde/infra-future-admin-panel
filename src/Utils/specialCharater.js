export const specialCharacters = (str) =>{
    return /[~`!#@$%&*+=\-_[\]\\';,/{}|\\":<>]/g.test(str);
}